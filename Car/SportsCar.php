<?php

  require_once "Car.php";
  require_once "CarModel.php";

  class SportsCar implements Car, CarModel {
    public $model;

    public function applyBreak() {
      echo "Applying brakes...<br>";     
    }
    public function increaseSpeed() {
      echo "Increasing speed...<br>";     
    }
    public function decreaseSpeed() {
      echo "Decreasing speed...<br>";     
    }
    public function setModel($model) {
      $this->model = $model;
    }
    public function getModel() {
      return $this->model;
    }
  }

?>
