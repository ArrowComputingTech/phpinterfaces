<?php

  interface Stock {
    function getPE();
    function getCurrentPrice();
    function buyMarket($marketAmount);
    function sellMarket($marketAmount);
    function buyLimit($limitPrice);
    function sellLimit($limitPrice);
  }

?>
