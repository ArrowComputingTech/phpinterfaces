<?php

  interface Phone {
    function makeCall($phoneNumber);
    function sendMessage($phoneNumber, $messageContent);
  }

?>
