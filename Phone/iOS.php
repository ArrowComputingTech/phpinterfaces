<?php

  require_once './Phone.php';

    class iOS implements Phone {
      public $phoneNumber;
      public $messageContent;
      function makeCall($phoneNumber) {
        echo "Dialing " . $this->phoneNumber . ".<br>";
      }

      function sendMessage($phoneNumber, $messageContent) {
        echo "Sending message to " . $this->phoneNumber . " with content: " . $this->messageContent . " <br>";
      }
    }

  $phone1 = new iOS;
  $phone1->phoneNumber = '501-438-9222';
  $phone1->messageContent = "Hello, phone world!";
  $phone1->makeCall($num);
  $phone1->sendMessage($phone1->phoneNumber, $phone1->messageContent);

?>
